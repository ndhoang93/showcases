<?php

namespace App\Http\Controllers;

use App\Services\ImageService;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    public function index() {
        return view('pages.upload-image');
    }

    public function store(Request $request) {
        try {
            $file = $request->file('image');
            ImageService::store($file);
            //TODO: do something else
        } catch (\Exception $exception) {
            //TODO: handle exception
        }
    }
}
