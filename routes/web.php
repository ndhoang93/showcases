<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageUploadController;

Route::get('upload-image', [ImageUploadController::class, 'index'])
    ->name('imageUpload.index');
Route::post('upload-image', [ImageUploadController::class, 'store'])
    ->name('imageUpload.store');

Route::get('/', function () {
    return view('welcome');
});
