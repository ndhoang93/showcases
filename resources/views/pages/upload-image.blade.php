@extends('layouts.master')

@section('content')
    <div class="container p-4">
        <h1 class="h2 mb-3">Demo upload image</h1>
        <div class="row">
            <div class="col-12">
                <div class="card p-3">
                    <form method="POST" action="{{ route('imageUpload.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <input type="file" name="image" class="form-control" aria-label="Upload" accept="image/*">
                            <button class="btn btn-outline-secondary" type="submit">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
